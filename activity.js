db.fruits.aggregate([

    {$match: {supplier: "Yellow Farms", price:{$lt:50}}},
    {$count: "itemsSuppliedByYellowFarms"}

]);

db.fruits.aggregate([

    {$match: {price: {$lt:30}}},
    {$count: "itemsCheaperThan30"}

]);

db.fruits.aggregate([

    {$match: {supplier: "Yellow Farms"}},
    {$group: {_id: "avgPriceOfFruitsOfYellowFarms", avgPrice:{$avg:"$price"}}}

]);

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "highestPriceByRedFarmsInc", maxPrice: {$max: "$price"}}}

]);

db.fruits.aggregate([

    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id: "lowestPriceByRedFarmsInc", minPrice: {$min: "$price"}}}

]);